﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueueController : MonoBehaviour
{
    public GameObject clientGenerator;
    public List<ClientController> queueList = new List<ClientController>();

    private void Update()
    {
        FetchChildren();
        if (queueList[0].transform.position == new Vector3(0, 0, 0))
        {
            GameStats.Timer -= Time.deltaTime;
        }
        if (queueList[0].transform.position == new Vector3(0, 0, -2f))
        {
            queueList.Remove(queueList[0]);
            Destroy(transform.GetChild(0).gameObject);
        }
        CheckClientsCondition();
        if (!CheckPos())
        {
            if (queueList[3].transform.position.z <= 5.5f)
                clientGenerator.GetComponent<ClientGenerator>().SpawnClient();
        }
        if (Input.GetKeyDown(KeyCode.W))
            queueList[0].IsHealed = true;
    }

    private bool CheckPos()
    {
        Vector3 pos = new Vector3(0, 0, 6f);
        foreach (ClientController clientController in queueList)
        {
            if (clientController.transform.position == pos)
            {
                return true;
            }
        }

        return false;
    }

    private void MoveQueue()
    {
        foreach (var client in queueList)
        {
            client.QueueMove = true;
        }
    }
    
    private void CheckClientsCondition()
    {
        if (GameStats.Timer <= 0f)
        {
            MoveQueue();
            GameStats.Score -= 100;
            GameStats.Timer = 25f;
            // clientGenerator.GetComponent<ClientGenerator>().SpawnClient();
        }
        if (queueList[0].IsHealed)
        {
            MoveQueue();
            GameStats.Score += Mathf.Round((100 * (GameStats.Timer * 0.01f)) * 1f) / 1f;
            GameStats.Timer = 25f;
            // clientGenerator.GetComponent<ClientGenerator>().SpawnClient();
        }
    }

    private void FetchChildren()
    {
        foreach (Transform child in transform)
        {
            if (!queueList.Contains(child.gameObject.GetComponent<ClientController>()))
            {
                queueList.Add(child.gameObject.GetComponent<ClientController>());
            }
        }
    }
}