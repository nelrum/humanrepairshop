﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    private Text uiText;

    private void Start()
    {
        uiText = GetComponent<Text>();
    }

    void Update()
    {
        uiText.text = "Score: " + GameStats.Score;
    }
}
