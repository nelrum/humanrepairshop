﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameStats
{
    private static float timer = 25f;
    private static float score = 0;

    public static float Timer
    {
        get => timer;
        set => timer = value;
    }
    
    public static float Score
    {
        get => score;
        set => score = value;
    }
}
