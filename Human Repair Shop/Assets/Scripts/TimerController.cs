﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    private Text uiText;

    private void Start()
    {
        uiText = GetComponent<Text>();
    }

    void Update()
    {
        float rTimer = Mathf.Round(GameStats.Timer * 10f) / 10f;
        uiText.text = "Timer: " + rTimer + "s";
    }
}
