﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ClientGenerator : MonoBehaviour
{
    public List<GameObject> clientFaces;
    public List<string> dialogues;
    public GameObject queue;

    private void Start()
    {
        for (var i = 0; i != 4; i++) 
            SpawnClient();
    }

    private void Update()
    {
        transform.position = new Vector3(0, 0, 6f);
    }

    public void SpawnClient()
    {
        var num = Random.Range(0, clientFaces.Count);
        var strNum = Random.Range(0, dialogues.Count);
        var client = Instantiate(clientFaces[num], transform.position, queue.transform.rotation, queue.transform);
        client.AddComponent<ClientController>();
        client.GetComponent<ClientController>().Dialogue = dialogues[strNum];
        transform.position += transform.forward * 2f;
    }
}
