﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClientController : MonoBehaviour
{
    public bool isHealed;

    private string dialogue;
    private bool queueMove;
    private Vector3 nextQueuePosition;
    private Text uiDialogue;
    private bool lookAtDoctor;

    public bool IsHealed
    {
        get => isHealed;
        set => isHealed = value;
    }

    public bool QueueMove
    {
        get => queueMove;
        set => queueMove = value;
    }

    public string Dialogue
    {
        get => dialogue;
        set => dialogue = value;
    }

    private void MoveTo(Vector3 to, float speed)
    {
        transform.position = Vector3.MoveTowards(transform.position, to, speed * Time.deltaTime);
    }
    
    private void Awake()
    {
        isHealed = false;
    }
    
    private void Start()
    {
        uiDialogue = transform.Find("Canvas").GetChild(0).GetComponent<Text>();
        uiDialogue.text = dialogue;
        nextQueuePosition = new Vector3(0, 0, transform.position.z - 2);
    }

    private bool CompareTwoVector(Vector3 from, Vector3 to)
    {
        return from.x <= to.x && from.y <= to.y && from.z <= to.z;
    }

    void RotateMesh(bool state, float rotationDegree)
    {
        if (lookAtDoctor == state)
        {
            transform.GetChild(0).rotation = Quaternion.Euler(0, rotationDegree, 0);
            lookAtDoctor = !state;
        }
    }

    private void Update()
    {
        if (CompareTwoVector(transform.position, new Vector3(0, 0, -0.01f))) 
        {
            RotateMesh(true, 180);
            uiDialogue.text = "YOU'RE AMAZING!";
        }

        if (transform.position == Vector3.zero)
        {
            RotateMesh(false, 90);
        }
        if (queueMove)
        {
            isHealed = false;
            if (transform.position == nextQueuePosition)
                queueMove = false;
            MoveTo(nextQueuePosition, 1f);
        }
        else
        {
            nextQueuePosition = new Vector3(0, 0, transform.position.z - 2);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "MagicalCube")
        {
            isHealed = true;
            Console.Write("Cube detected!");
        }
    }
}
